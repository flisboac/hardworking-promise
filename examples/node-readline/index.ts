// tslint:disable:no-console
// tslint:disable:no-implicit-dependencies

import { hardworking, simple } from 'hardworking-promise';
import * as readline from 'readline';

const io = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

io.on('SIGINT', () => {
    io.close();
});

function question(headline: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
        try {
            io.question(headline, answer => resolve(answer));
        } catch (err) {
            return reject(err);
        }
    });
}

const expectedMessage = 'hello, there!';

async function ask() {
    console.log("Wait for it...");
    try {

        // By default, "resolves" (i.e. finishes when resolving into)
        // anything that's not undefined.

        const value = await hardworking.loop(async () => {
            const answer = await question(`Say "${expectedMessage}": `);

            if (answer === expectedMessage) {
                return answer;
            }

            console.log("Wrong answer! Let's try again in ");
            let counter = 5;
            while (counter--) {
                console.log(`${counter + 1}... `);
                await simple.delay(1000);
            }
        }, 1000);

        // hardworking task finished!

        console.log(`*** Answered: ${value}`);

    } catch (err) {
        io.close();
        return Promise.reject(err);
    }
}

async function main() {
    await ask();
    console.log("It's over, my friend!");
    io.close();
}

main();
