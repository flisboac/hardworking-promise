"use strict";
// tslint:disable:no-console
// tslint:disable:no-implicit-dependencies
Object.defineProperty(exports, "__esModule", { value: true });
const hardworking_promise_1 = require("hardworking-promise");
const readline = require("readline");
const io = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});
io.on('SIGINT', () => {
    io.close();
});
function question(headline) {
    return new Promise((resolve, reject) => {
        try {
            io.question(headline, answer => resolve(answer));
        }
        catch (err) {
            return reject(err);
        }
    });
}
const expectedMessage = 'hello, there!';
async function ask() {
    console.log("Wait for it...");
    try {
        // By default, "resolves" (i.e. finishes when resolving into)
        // anything that's not undefined.
        const value = await hardworking_promise_1.hardworking.loop(async () => {
            const answer = await question(`Say "${expectedMessage}": `);
            if (answer === expectedMessage) {
                return answer;
            }
            console.log("Wrong answer! Let's try again in ");
            let counter = 5;
            while (counter--) {
                console.log(`${counter + 1}... `);
                await hardworking_promise_1.simple.delay(1000);
            }
        }, 1000);
        // hardworking task finished!
        console.log(`*** Answered: ${value}`);
    }
    catch (err) {
        io.close();
        return Promise.reject(err);
    }
}
async function main() {
    await ask();
    console.log("It's over, my friend!");
    io.close();
}
main();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsNEJBQTRCO0FBQzVCLDBDQUEwQzs7QUFFMUMsNkRBQTBEO0FBQzFELHFDQUFxQztBQUVyQyxNQUFNLEVBQUUsR0FBRyxRQUFRLENBQUMsZUFBZSxDQUFDO0lBQ2hDLEtBQUssRUFBRSxPQUFPLENBQUMsS0FBSztJQUNwQixNQUFNLEVBQUUsT0FBTyxDQUFDLE1BQU07Q0FDekIsQ0FBQyxDQUFDO0FBRUgsRUFBRSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFO0lBQ2pCLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztBQUNmLENBQUMsQ0FBQyxDQUFDO0FBRUgsU0FBUyxRQUFRLENBQUMsUUFBZ0I7SUFDOUIsT0FBTyxJQUFJLE9BQU8sQ0FBUyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtRQUMzQyxJQUFJO1lBQ0EsRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztTQUNwRDtRQUFDLE9BQU8sR0FBRyxFQUFFO1lBQ1YsT0FBTyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDdEI7SUFDTCxDQUFDLENBQUMsQ0FBQztBQUNQLENBQUM7QUFFRCxNQUFNLGVBQWUsR0FBRyxlQUFlLENBQUM7QUFFeEMsS0FBSyxVQUFVLEdBQUc7SUFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDOUIsSUFBSTtRQUVBLDZEQUE2RDtRQUM3RCxpQ0FBaUM7UUFFakMsTUFBTSxLQUFLLEdBQUcsTUFBTSxpQ0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksRUFBRTtZQUM1QyxNQUFNLE1BQU0sR0FBRyxNQUFNLFFBQVEsQ0FBQyxRQUFRLGVBQWUsS0FBSyxDQUFDLENBQUM7WUFFNUQsSUFBSSxNQUFNLEtBQUssZUFBZSxFQUFFO2dCQUM1QixPQUFPLE1BQU0sQ0FBQzthQUNqQjtZQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUNBQW1DLENBQUMsQ0FBQztZQUNqRCxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUM7WUFDaEIsT0FBTyxPQUFPLEVBQUUsRUFBRTtnQkFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsT0FBTyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ2xDLE1BQU0sNEJBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDNUI7UUFDTCxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCw2QkFBNkI7UUFFN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsS0FBSyxFQUFFLENBQUMsQ0FBQztLQUV6QztJQUFDLE9BQU8sR0FBRyxFQUFFO1FBQ1YsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ1gsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0tBQzlCO0FBQ0wsQ0FBQztBQUVELEtBQUssVUFBVSxJQUFJO0lBQ2YsTUFBTSxHQUFHLEVBQUUsQ0FBQztJQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQztJQUNyQyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDZixDQUFDO0FBRUQsSUFBSSxFQUFFLENBQUMifQ==