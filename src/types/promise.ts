
export type TPromiseExecutor<T> = (resolve: (result: T) => any, reject: (reason: any) => any) => any;

export interface IThenable<T> extends PromiseLike<T> {
    then<TResult1 = T, TResult2 = never>(
        onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null,
        onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null,
    ): IThenable<TResult1 | TResult2>;
}

export interface IPromise<T> extends IThenable<T> {
    then<TResult1 = T, TResult2 = never>(
        onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null,
        onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null,
    ): IPromise<TResult1 | TResult2>;
    catch<TResult = never>(
        onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | null | undefined,
    ): IPromise<T | TResult>;
}

export interface IPromiseConstructor {
    new <T>(executor: TPromiseExecutor<T>): IPromise<T>;
    resolve(): IPromise<void>;
    resolve<T>(result?: T | IPromise<T>): IPromise<T>;
    reject(reason: any): IPromise<never>;
}
