import {
    expectedValue,
    makeDefaultTimeout,
    makeErrorTimeout,
    maxTimeout,
    shortTimeout,
} from 'spec/definitions';

import { OperationCanceledError, simple } from 'lib';

describe('a simple timeout thenable', () => {

    describe('that should resolve to a value', () => {

        it('is resolved correctly', async () => {
            try {
                const { executor, timeout } = makeDefaultTimeout();
                jest.runAllTimers();
                const value = await timeout;
                expect(executor).toBeCalledTimes(1);
                expect(value).toBe(expectedValue);
            } catch (err) {
                fail(`Should not have failed!` +
                    ` Type: '${err.constructor.name}'` +
                    `, message: '${err.message}'` +
                    `, reason: '${err.reason}'`);
            }
        });
    });
});

describe('two different timeout thenable', () => {
    const t1 = makeDefaultTimeout(shortTimeout, false);
    const t2 = makeDefaultTimeout(shortTimeout, false);

    it('have different timeout IDs', () => {
        expect(t1.timeout.id).not.toBe(t2.timeout.id);
    });
});

describe('a timeout thenable', () => {

    describe('that should reject', () => {

        it('calls its executor only once', async () => {
            const { executor, timeout } = makeDefaultTimeout();
            await timeout;
            expect(executor).toBeCalled();
            expect(executor).toBeCalledTimes(1);
        });

        it('rejects when an error is thrown', () => {
            const { timeout } = makeErrorTimeout();
            const catcher = jest.fn(err => expect(err).toBeInstanceOf(Error));
            timeout.catch(catcher).then(() => expect(catcher).toBeCalledTimes(1));
        });
    });

    describe('that is canceled', async () => {
        const expectedTimeoutId = 1;
        let receivedTimeoutId: number;
        const setTimeout = jest.fn((...args: any[]) => expectedTimeoutId);
        const clearTimeout = jest.fn((timeoutId: number) => receivedTimeoutId = timeoutId);
        const implementation = {
            setTimeout,
            clearTimeout,
        };

        const timeout = simple.timeout(() => undefined, shortTimeout, { implementation });

        timeout
            .then(() => fail("Should have failed!"))
            .catch(() => {
                it('is rejected', async () => {
                    expect(setTimeout).toBeCalledTimes(1);
                    try {
                        fail("expected timeout to reject");
                    } catch (err) {
                        expect(err).toBeInstanceOf(OperationCanceledError);
                        expect(err.reason).toBe(undefined);
                    }
                });

                it('calls clearTimeout once with the correct ID', async () => {
                    expect(receivedTimeoutId).toBe(expectedTimeoutId);
                    expect(clearTimeout).toBeCalledTimes(1);
                });
            });

        try {
            timeout.cancel();
            await timeout;
        } catch (e) {}
    });

    describe('that is still running', () => {

        it('can be canceled', async () => {
            const { executor, timeout } = makeDefaultTimeout(maxTimeout);
            timeout.cancel();
            jest.runAllTimers();
            try {
                await timeout;
                fail("expected timeout to reject");
            } catch (err) {
                expect(err).toBeInstanceOf(OperationCanceledError);
                expect(err.reason).toBe(undefined);
            }
            expect(executor).toBeCalledTimes(0);
        });

        it('can be canceled with a reason', async () => {
            const { executor, timeout } = makeDefaultTimeout(maxTimeout);
            timeout.cancel(expectedValue);
            jest.runAllTimers();
            try {
                await timeout;
                fail("expected timeout to reject");
            } catch (err) {
                expect(err).toBeInstanceOf(OperationCanceledError);
                expect(err.reason).toBe(expectedValue);
            }
            expect(executor).toBeCalledTimes(0);
        });

        it('can be canceled with a message and a reason', async () => {
            const message = `Message: ${expectedValue}`;
            const { executor, timeout } = makeDefaultTimeout(maxTimeout);
            timeout.cancel(message, expectedValue);
            jest.runAllTimers();
            try {
                await timeout;
                fail("expected timeout to reject");
            } catch (err) {
                expect(err).toBeInstanceOf(OperationCanceledError);
                expect(err.message).toBe(message);
                expect(err.reason).toBe(expectedValue);
            }
            expect(executor).toBeCalledTimes(0);
        });
    });
});
