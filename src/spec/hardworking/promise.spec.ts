import {
    hardworking,
    isHardworkingPromise,
    OperationCanceledError,
    simple,
} from 'lib';

import {
    expectedValue,
    makeTimeout,
    maxTimeout,
    shortTimeout,
} from 'spec/definitions';


// ---


describe('A canceled hardworking promise', async () => {
    const executor = jest.fn(() => simple.delay(maxTimeout));
    const promise = hardworking.promise(executor);

    promise
        .then(() => fail("Should have failed!"))
        .catch(() => {
            it('is rejected', async () => {
                expect(setTimeout).toBeCalledTimes(0);
                try {
                    fail("expected promise to reject");
                } catch (err) {
                    expect(err).toBeInstanceOf(OperationCanceledError);
                    expect(err.reason).toBe(undefined);
                }
            });
        });

    try {
        promise.cancel();
        await promise;
    } catch (e) {}
});

// Testing the case where the executor function resolves the hardworking promise in a single call
describe('a single-call hardworking promise (i.e. having an executor that resolves in a single call)', () => {

    describe('that should resolve to a value', () => {
        const makePromise = () => hardworking.promise(resolve => resolve(expectedValue));

        it('is a hardworking promise', async () => isHardworkingPromise(makePromise()));

        it('is resolved', () =>
            makePromise().then(value => expect(value).toBe(expectedValue))
        , maxTimeout);
    });

    describe('that should reject', () => {
        const makePromise = () => hardworking.promise((_, reject) => reject(expectedValue));

        it('is rejected', async () => {
            try {
                await makePromise();
                fail("'reject' function not called");
            } catch (err) {
                expect(err).toBe(expectedValue);
            }
        }, maxTimeout);

        it('is rejected (when the executor throws)', async () => {
            const promise = hardworking.promise(() => {
                throw new Error(expectedValue);
            });

            try {
                await promise;
                fail("Error not captured, or 'reject' function not called");
            } catch (err) {
                expect(err.message).toBe(expectedValue);
            }
        }, maxTimeout);
    });
});

// Testing the case where the executor function resolves the hardworking promise in a single call
describe('a multi-call hardworking promise (i.e. having an executor that resolves after multiple calls)', () => {

    describe('that should resolve to a value', () => {
        const maxCalls = 3;
        const makePromise = () => {
            let calls = 0;
            const executor = jest.fn((resolve: any) => {
                if (++calls === maxCalls) resolve(expectedValue);
            });
            const promise = hardworking.promise(executor);
            return { executor, promise };
        };

        it('is a hardworking promise', async () => isHardworkingPromise(makePromise()));

        it('is resolved correctly', () =>
            makePromise().promise.then(value => expect(value).toBe(expectedValue))
        , maxTimeout);

        it('calls its executor function by the correct amount of times', async () => {
            const { executor, promise } = makePromise();
            await promise;
            expect(executor).toBeCalledTimes(maxCalls);
        }, maxTimeout);

        it('keeps the correct call count in "info.executions" when executing the "onPending" hook', async () => {
            let calls = 0;
            let correctCount = true;
            const promise = hardworking.promise((resolve: any) => {
                if (++calls === maxCalls) resolve(expectedValue);
            }, {
                onPending: ({executions}) => (correctCount = correctCount && (executions === calls)),
            });
            await promise;
            expect(correctCount).toBe(true);
        }, maxTimeout);
    });

    describe('that should reject', () => {
        const makePromise = () => hardworking.promise((_, reject) => reject(expectedValue));

        it('is rejected', async () => {
            try {
                await makePromise();
                fail("'reject' function not called");
            } catch (err) {
                expect(err).toBe(expectedValue);
            }
        }, maxTimeout);

        it('is rejected (when the executor throws)', async () => {
            const promise = hardworking.promise(() => {
                throw new Error(expectedValue);
            });

            try {
                await promise;
                fail("Error not captured, or 'reject' function not called");
            } catch (err) {
                expect(err.message).toBe(expectedValue);
            }
        }, maxTimeout);
    });
});

// Testing the case where the executor function returns a promise, and the resolution functions
// (resolve/reject) will be called as part of the returned promise's resolution.
describe('a single-call executor-delayed hardworking promise'
+ ' (i.e. having an executor that resolves in a single promise-returning executor call)', () => {

    describe('that should resolve to a value', () => {
        const makePromise = () => hardworking.promise(resolve =>
            makeTimeout(() => resolve(expectedValue), shortTimeout).timeout,
        );
        it('is a hardworking promise', async () => isHardworkingPromise(makePromise()));

        it('is resolved', () =>
            makePromise().then(value => expect(value).toBe(expectedValue))
        , maxTimeout);
    });

    describe('that should resolve by means of the "onPending" hook', () => {

        it('is resolved with the "onPending"\'s rejection value', async () => {
            const promise = hardworking.promise(
                () => undefined, {
                    onPending: ({resolve}) => resolve(expectedValue),
                });
            const resolvedValue = await promise;
            expect(resolvedValue).toBe(expectedValue);
        }, maxTimeout);
    });

    describe('that should reject', () => {
        const makePromise = () => hardworking.promise((_, reject) =>
            makeTimeout(() => reject(expectedValue), shortTimeout).timeout,
        );

        it('is rejected', async () => {
            try {
                await makePromise();
                fail("the promise's caught value was probably ignored");
            } catch (err) {
                expect(err).toBe(expectedValue);
            }
        }, maxTimeout);

        it('is rejected (when the executor-returned promise rejects)', async () => {
            const promise = hardworking.promise(() =>
                makeTimeout(() => { throw new Error(expectedValue); }, shortTimeout).timeout,
            );

            try {
                await promise;
                fail("Thrown error was not captured, promise not rejected");
            } catch (err) {
                expect(err.message).toBe(expectedValue);
            }
        }, maxTimeout);
    });

    describe('that should reject by means of the "onPending" hook', () => {

        it('is rejected with the "onPending"\'s rejection value', async () => {
            try {
                const promise = hardworking.promise(
                    () => undefined, {
                        onPending: ({reject}) => reject(expectedValue),
                    });
                await promise;
                fail("the promise's caught value was probably ignored");
            } catch (err) {
                expect(err).toBe(expectedValue);
            }
        }, maxTimeout);
    });
});

// Testing the case where the executor function resolves the hardworking promise in a single call
describe('a multi-call executor-delayed hardworking promise'
+ ' (i.e. having an executor that resolves after multiple promise-returning executor calls)', () => {

    describe('that should resolve to a value', () => {
        const maxCalls = 3;
        const makePromise = () => {
            let calls = 0;
            const executor = jest.fn((resolve: any) => {
                if (++calls === maxCalls) resolve(expectedValue);
            });
            const promise = hardworking.promise(executor);
            return { executor, promise };
        };

        it('is a hardworking promise', async () => isHardworkingPromise(makePromise()));

        it('is resolved correctly', () =>
            makePromise().promise.then(value => expect(value).toBe(expectedValue))
        , maxTimeout);

        it('calls its executor function by the correct amount of times', async () => {
            const { executor, promise } = makePromise();
            await promise;
            expect(executor).toBeCalledTimes(maxCalls);
        }, maxTimeout);
    });

    describe('that should reject', () => {
        const makePromise = () => hardworking.promise((_, reject) => reject(expectedValue));

        it('is rejected', async () => {
            try {
                await makePromise();
                fail("'reject' function not called");
            } catch (err) {
                expect(err).toBe(expectedValue);
            }
        }, maxTimeout);

        it('is rejected (when the executor throws)', async () => {
            const promise = hardworking.promise(() => {
                throw new Error(expectedValue);
            });

            try {
                await promise;
                fail("Error not captured, or 'reject' function not called");
            } catch (err) {
                expect(err.message).toBe(expectedValue);
            }
        }, maxTimeout);
    });
});
