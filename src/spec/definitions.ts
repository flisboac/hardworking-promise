import { simple } from 'lib';

//
// CONSTANTS
//

export const shortTimeout = 50;
export const maxTimeout = 1000;
export const expectedValue = 'hello, there';


// HELPERS


export const makeDefaultTimeout = (interval = shortTimeout, runTimers = true) => {
    return makeTimeout(() => expectedValue, interval, runTimers);
};

export const makeTimeout = <T>(body: (() => T), interval = shortTimeout, runTimers = true) => {
    const executor = jest.fn(body);
    const timeout = simple.timeout(executor, interval);
    if (runTimers) jest.runAllTimers();
    return { executor, timeout };
};

export const makeErrorTimeout = (interval = shortTimeout, thrower = () => { throw new Error(expectedValue); }) => {
    const body = jest.fn(thrower);
    const timeout = simple.timeout(body, interval);
    jest.runAllTimers();
    return { body, timeout };
};


// ----


jest.useFakeTimers();
