import { IPromise } from "types";

export interface ICancelable {
    cancel(): void;
    cancel(reason: any): void;
    cancel(message: string, reason: any): void;
}

export interface IPromiseWrapper<T> extends IPromise<T> {
    readonly promise: IPromise<T>;
}

