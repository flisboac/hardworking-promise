import {
    IPromise,
    IPromiseConstructor,
} from "types";

import {
    ICancelable,
    IPromiseWrapper,
} from './common';


//
// simple.timeout
//


export type FSetTimeout = (body: ((...args: any[]) => any), timeout: number, ...args: any[]) => number;
export type FClearTimeout = (timeoutId: number) => any;

export interface ITimeoutPromise<T = any> extends IPromiseWrapper<T>, ICancelable {
    readonly id: number;
}

export interface ITimeoutPromiseOptions<T = any> {
    timeout: number;
    Promise: IPromiseConstructor;
    implementation: {
        setTimeout: FSetTimeout;
        clearTimeout: FClearTimeout;
    };
    args: any[];
}

export type FTimeoutBody<T = any> = (...args: any[]) => T;

