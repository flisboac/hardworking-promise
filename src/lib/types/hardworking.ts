import {
    IPromise,
    IPromiseConstructor,
    IThenable,
} from "types";

import {
    ICancelable,
    IPromiseWrapper,
} from './common';

//
// hardworking.promise
//

export enum EHardworkingStatus {
    PENDING = 'PENDING',
    FULFILLED = 'FULFILLED',
    REJECTED = 'REJECTED',
}

export interface IHardworkingOptions<T> {
    onPending: FOnPendingHardwork<T> | Array< FOnPendingHardwork<T> >;
    Promise: IPromiseConstructor;
    delay: number | IThenable<T>;
}

export interface IHardworkingInfo<T> {
    executions: number;
    resolve: TResolver<T>;
    reject: TRejector;
}

export interface IHardworkingPromise<T> extends IPromiseWrapper<T>, ICancelable {
}

export type TExecutor<T> = (resolve: (result: T) => any, reject: (reason: any) => any) => any;
export type TResult = undefined | boolean | IThenable<any>;
export type TResolver<T> = (result: T) => any;
export type TRejector = (reason?: any) => any;
export type THardworkingExecutor<T> = (resolve: TResolver<T>, reject: TRejector) => void | TResult;
export type FOnPendingHardwork<T> = (info: IHardworkingInfo<T>) => void | TResult;


//
// hardworking.loop
//


export type FOnCheckLoop<T> = (resolvedValue: T) => boolean;

export interface IHardworkingLoopOptions<T>
extends Pick<IHardworkingOptions<T>, Exclude<keyof IHardworkingOptions<T>, "onPending">> {
    onCheck: FOnCheckLoop<T>;
}

export type FLoopBody<T> = () => T | IPromise<T>;
