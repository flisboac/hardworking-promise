
interface IOperationCanceledErrorInfo {
    message: string;
    reason: any;
}

export class OperationCanceledError extends Error {

    public readonly reason?: any;

    constructor(message: string);
    constructor(message: string, reason: any);
    constructor(info: Partial<IOperationCanceledErrorInfo>);
    constructor(...args: any[]) {
        const info: Partial<IOperationCanceledErrorInfo> = {};

        if (args.length > 1) {
            info.message = args[0];
            info.reason = args[1];

        } else if (args.length === 1) {
            if (typeof args[0] === 'string') {
                info.message = args[0];

            } else {
                const providedInfo = args[0] as Partial<IOperationCanceledErrorInfo>;
                info.message = providedInfo.message;
                info.reason = providedInfo.reason;
            }
        }

        const reasonMessage = info.reason !== undefined ? String(info.reason) : "Operation cancelled.";
        info.message = info.message || reasonMessage;

        super(info.message);
        this.reason = info.reason;
    }
}
