
export * from 'types';

export * from './types';

export * from './error';

export * from './isPromise';

import * as simple from './simple';

import * as hardworking from './hardworking';

export const delay = simple.delay;
export const timeout = simple.timeout;
export const hardworkingPromise = hardworking.promise;
export const hardworkingLoop = hardworking.loop;

export const { HardworkingPromise } = hardworking;

export {
    simple,
    hardworking,
};
