import {
    IPromise,
    IThenable,
} from "types";

import {
    EHardworkingStatus,
    IHardworkingOptions,
    IHardworkingPromise,
    THardworkingExecutor,
    TRejector,
    TResolver,
    TResult,
} from "lib/types";

import { DefaultPromise, hardworkingSymbol } from "lib/definitions";
import { OperationCanceledError } from "lib/error";
import { isPromiseWrapper, isThenable } from "lib/isPromise";
import * as util from "lib/simple";


// ---


/** @hide */
function justProceed() {}

export class HardworkingPromise<T> implements IHardworkingPromise<T> {

    public readonly [hardworkingSymbol] = true;
    public readonly promise: IPromise<T>;

    private _reject?: (reason?: any) => any;

    constructor(executor: THardworkingExecutor<T>);
    constructor(
        executor: THardworkingExecutor<T>,
        timeout: number,
    );
    constructor(
        executor: THardworkingExecutor<T>,
        options: Partial<IHardworkingOptions<T>>,
    );
    constructor(
        executor: THardworkingExecutor<T>,
        timeout: number,
        options: Partial<IHardworkingOptions<T>>,
    );
    constructor(
        executor: THardworkingExecutor<T>,
        delay_?: number | Partial<IHardworkingOptions<T>>,
        options_?: Partial<IHardworkingOptions<T>>,
    )
    /** @hide */
    constructor(
        executor: THardworkingExecutor<T>,
        delay_?: number | Partial<IHardworkingOptions<T>>,
        options_?: Partial<IHardworkingOptions<T>>,
    ) {
        const options = typeof delay_ === 'object' ? delay_
            : (options_ || {});
        const delay = typeof delay_ === 'number' ? delay_
            : isThenable(options.delay) ? 0
            : (options.delay || 0);

        options.Promise = options.Promise || DefaultPromise;
        options.onPending = options.onPending || justProceed;

        const delayPromise: IThenable<any> | undefined =
            isThenable(options.delay) ? options.delay
            : delay > 0 ? util.delay(delay)
            : undefined;
        const PromiseClass = options.Promise;
        const pendingHandlers = options.onPending instanceof Array
            ? options.onPending.length >= 0 ? options.onPending : [ justProceed ]
            : [ options.onPending ];

        let executionsCount = 0;
        let status = EHardworkingStatus.PENDING;
        let expertResolve: TResolver<T>;
        let expertReject: TRejector;

        const hardworkingResolve = (result: T) => {
            expertResolve(result);
            status = EHardworkingStatus.FULFILLED;
        };

        const hardworkingReject = (reason: any) => {
            expertReject(reason);
            status = EHardworkingStatus.REJECTED;
        };

        const safeHardworkingReject = (reason: any) => {
            if (status === EHardworkingStatus.PENDING) {
                hardworkingReject(reason);
            }
        };

        this._reject = safeHardworkingReject;

        const handleError = (reason: any) => {
            safeHardworkingReject(reason);
            return PromiseClass.reject(reason);
        };

        const isPending = (currentStatus = status) => currentStatus === EHardworkingStatus.PENDING;

        const ignoreAll = () => undefined;

        const handleHardworkingResult = (hardworkingResult: TResult) => {
            if (isPromiseWrapper(hardworkingResult)) {
                return hardworkingResult.promise.then(() => status);
            }
            return PromiseClass.resolve(hardworkingResult).then(() => status);
        };

        const execute = () => {
            return executor(hardworkingResolve, hardworkingReject);
        };

        this.promise = new PromiseClass<T>((resolve, reject) => {

            expertResolve = resolve;
            expertReject = reject;

            const run = () => {
                const executions = ++executionsCount;
                const info = {
                    executions,
                    resolve: hardworkingResolve,
                    reject: hardworkingReject,
                };

                const handlePending = (
                    index = 0,
                    currentPromise = PromiseClass.resolve<EHardworkingStatus>(),
                ): IPromise<EHardworkingStatus> => {
                    if (index >= pendingHandlers.length) {
                        return handleHardworkingResult(currentPromise);
                    }
                    return currentPromise
                        .then(() => isPending()
                            ? handlePending(index + 1, PromiseClass.resolve(pendingHandlers[index](info)))
                            : handlePending(pendingHandlers.length, currentPromise),
                        );
                };

                PromiseClass.resolve()
                    .then(execute, handleError)
                    .then(handleHardworkingResult, handleError)
                    .then(currentStatus => isPending(currentStatus) ? handlePending() : currentStatus, handleError)
                    .then(currentStatus => isPending(currentStatus) && run(), handleError)
                    .then(ignoreAll, ignoreAll);
            };

            if (delayPromise) {
                delayPromise.then(run);
            } else {
                run();
            }
        });
    }

    public then<TResult1 = T, TResult2 = never>(
        onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | null | undefined,
        onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | null | undefined,
    ): IPromise<TResult1 | TResult2> {
        return this.promise.then(onfulfilled, onrejected);
    }

    public catch<TResultValue = never>(
        onrejected?: ((reason: any) => TResultValue | PromiseLike<TResultValue>) | null | undefined,
    ): IPromise<T | TResultValue> {
        return this.promise.catch(onrejected);
    }

    public cancel(): void;
    public cancel(reason: any): void;
    public cancel(message: string, reason: any): void;
    public cancel(...args: any[]): void {
        let message: string | undefined;
        let reason: any;

        if (args.length > 1) {
            message = args[0];
            reason = args[1];
        } else if (args.length === 1) {
            reason = args[0];
        }

        if (this._reject) {
            const reject = this._reject;
            this._reject = undefined;
            reject(new OperationCanceledError({ message, reason }));
        }
    }
}

export function promise<T>(
    executor: THardworkingExecutor<T>,
): IHardworkingPromise<T>;
export function promise<T>(
    executor: THardworkingExecutor<T>,
    timeout: number,
): IHardworkingPromise<T>;
export function promise<T>(
    executor: THardworkingExecutor<T>,
    options: Partial<IHardworkingOptions<T>>,
): IHardworkingPromise<T>;
export function promise<T>(
    executor: THardworkingExecutor<T>,
    timeout: number,
    options: Partial<IHardworkingOptions<T>>,
): IHardworkingPromise<T>;
export function promise<T>(
    executor: THardworkingExecutor<T>,
    delay_?: number | Partial<IHardworkingOptions<T>>,
    options_?: Partial<IHardworkingOptions<T>>,
): IHardworkingPromise<T> {
    return new HardworkingPromise(executor, delay_, options_);
}
