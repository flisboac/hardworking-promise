import { FLoopBody, IHardworkingLoopOptions } from "lib/types";
import { IPromise } from "types";

import { DefaultPromise } from "lib/definitions";
import { promise } from "./promise";

/** @hide */
function justCheckNonUndefined(value: any): boolean {
    return value !== undefined;
}

export function loop<T = any>(body: FLoopBody<T>): IPromise<Exclude<T, undefined>>;
export function loop<T = any>(body: FLoopBody<T>, timeout: number): IPromise<Exclude<T, undefined>>;
export function loop<T = any>(body: FLoopBody<T>, options: Partial<IHardworkingLoopOptions<T>>): IPromise<T>;
export function loop<T = any>(
    body: FLoopBody<T>,
    timeout: number,
    options: Partial<IHardworkingLoopOptions<T>>,
): IPromise<T>;
export function loop<T = any>(
    body: FLoopBody<T>,
    timeout_?: number | Partial<IHardworkingLoopOptions<T>>,
    options_?: Partial<IHardworkingLoopOptions<T>>,
) {
    const options = typeof timeout_ === 'object' ? timeout_ : (options_ || {});
    const timeout = typeof timeout_ === 'number' ? timeout_ : (options.delay || 0);

    options.Promise = options.Promise || DefaultPromise;
    options.onCheck = options.onCheck || justCheckNonUndefined;

    const { Promise, onCheck } = options;

    return promise<T>(
        (resolve, reject) => {
            return Promise.resolve(body()).then(value => {
                const shouldStop = onCheck(value);
                if (shouldStop) {
                    resolve(value);
                    return;
                }
            }, reject);
        },
        { ...options, delay: timeout },
    );
}

