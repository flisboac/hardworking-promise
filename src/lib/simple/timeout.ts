import { IPromise } from "types";

import {
    FClearTimeout,
    FTimeoutBody,
    ITimeoutPromise,
    ITimeoutPromiseOptions,
} from "lib/types";

import { DefaultPromise, DefaultSetTimeout } from "lib/definitions";
import { OperationCanceledError } from "lib/error";


export class SimpleTimeout<T = any> implements ITimeoutPromise<T> {

    public readonly interval: number;
    public readonly promise: IPromise<T>;

    public [Symbol.toStringTag]: 'Timeout';

    private _id: number;
    private readonly _clearTimeout: FClearTimeout;
    private _reject?: (reason?: any) => any;

    constructor(
        body: FTimeoutBody<T>,
        msToWait: number | (Partial<ITimeoutPromiseOptions<T>> & Pick<ITimeoutPromiseOptions<T>, "timeout">),
        options?: Partial<ITimeoutPromiseOptions<T>>,
    ) {
        options = typeof msToWait === 'object' ? msToWait : (options || {});
        const PromiseClass = options.Promise || DefaultPromise;
        const args = options.args || [];
        const detectedTimeout: number =
            typeof msToWait === 'number' ? msToWait
            : typeof options.timeout === 'number' ? options.timeout
            : -1;
        const implementation = options.implementation || DefaultSetTimeout;
        this._clearTimeout = implementation.clearTimeout;

        if (detectedTimeout < 0) {
            throw new Error("Timeout was not provided or is not a valid value.");
        }

        this.interval = detectedTimeout;
        this._id = 0;
        this.promise = new PromiseClass((resolve, reject) => {
            this._reject = reject;
            this._id = implementation.setTimeout(() => {
                this._reject = undefined;
                try {
                    const result = body(...args);
                    resolve(result);
                } catch (err) {
                    reject(err);
                }
            }, detectedTimeout);
        });
    }

    public get id() {
        return this._id;
    }

    public cancel(): void;
    public cancel(reason: any): void;
    public cancel(message: string, reason: any): void;
    public cancel(...args: any[]): void {
        let message: string | undefined;
        let reason: any;

        if (args.length > 1) {
            message = args[0];
            reason = args[1];
        } else if (args.length === 1) {
            reason = args[0];
        }

        if (this._id && this._reject) {
            const reject = this._reject;
            this._reject = undefined;
            this._clearTimeout(this._id);
            reject(new OperationCanceledError({ message, reason }));
        }
    }

    public then<TResult1 = T, TResult2 = never>(
        onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | null | undefined,
        onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | null | undefined,
    ): IPromise<TResult1 | TResult2> {
        return this.promise.then(onfulfilled, onrejected);
    }

    public catch<TResult = never>(
        onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | null | undefined,
    ): IPromise<T | TResult> {
        return this.promise.catch(onrejected);
    }
}

export function timeout<T = any>(
    body: FTimeoutBody<T>,
    msToWait: number | (Partial<ITimeoutPromiseOptions> & Pick<ITimeoutPromiseOptions, "timeout">),
    options?: Partial<ITimeoutPromiseOptions>,
) {
    return new SimpleTimeout(body, msToWait, options);
}
