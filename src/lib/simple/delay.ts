import { ITimeoutPromiseOptions } from "lib/types";
import { SimpleTimeout } from "./timeout";

const justNoop = () => undefined;

export function delay(
    timeout: number | (Partial<ITimeoutPromiseOptions> & Pick<ITimeoutPromiseOptions, "timeout">),
    options?: Partial<ITimeoutPromiseOptions>,
) {
    return new SimpleTimeout<void>(justNoop, timeout, options);
}
