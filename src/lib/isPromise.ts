import { IPromise, IThenable } from "types";
import { hardworkingSymbol } from "./definitions";
import { IHardworkingPromise, IPromiseWrapper } from "./types";

function isObjectLike(value: any): value is object {
    return typeof value === 'object';
}

export function isPromiseWrapper<T = any>(value: any): value is IPromiseWrapper<T> {
    return isObjectLike(value) && 'promise' in value;
}

export function isCancelablePromise<T = any>(value: any): value is IPromise<T> {
    return isPromise(value) && 'cancel' in value;
}

export function isPromise<T = any>(value: any): value is IPromise<T> {
    return isThenable(value) && 'catch' in value;
}

export function isThenable<T = any>(value: any): value is IThenable<T> {
    return isObjectLike(value) && 'then' in value;
}

export function isHardworkingPromise<T = any>(value: any): value is IHardworkingPromise<T> {
    return isPromise(value) && (value as any)[hardworkingSymbol] === true;
}
