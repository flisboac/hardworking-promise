
//
// hardworking.loop
//


export const hardworkingSymbol: unique symbol = Symbol("hardworking.promise");
export const DefaultPromise = Promise;
export const DefaultSetTimeout = { setTimeout, clearTimeout };
